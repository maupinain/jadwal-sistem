<?php

use App\Http\Controllers\JadwalController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('conten');
// });

Route::get('/', [JadwalController::class, 'index']);
Route::get('detail-jadwal/{id}', [JadwalController::class, 'show']);
Route::get('buat-jadwal', [JadwalController::class, 'create']);
Route::post('simpan-jadwal', [JadwalController::class, 'store']);
Route::get('edit-jadwal/{id}', [JadwalController::class, 'edit']);
Route::post('update-jadwal/{id}', [JadwalController::class, 'update']);
Route::get('delete-jadwal/{id}', [JadwalController::class, 'destroy']);
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
