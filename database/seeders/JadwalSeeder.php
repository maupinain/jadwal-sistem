<?php

namespace Database\Seeders;

use App\Models\jadwal;
use Illuminate\Database\Seeder;

class JadwalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        jadwal::create([
            'mapel' => "agama islam",
            'guru' => "farhan,s.pd",
            'hari' => "senin",
            'jam' => "07.00-07.40",
        ]);
        jadwal::create([
            'mapel' => "matematika",
            'guru' => "maryani,s.pd",
            'hari' => "senin",
            'jam' => "07.40-08.20",
        ]);
    }
}
